import React from 'react'
import './SortByDropDown.css'
import { useGlobalContext } from '../../context/GlobalContext'

export default function SortByDropDown({type_of_dropdown, setSelectedSortByPopulation, setSelectedSortByArea, setSortByPopulationDropDown, setSortByAreaDropDown, selectedSortByPopulation,  selectedSortByArea }) {
    const {darkMode} = useGlobalContext()

    const sortByPopulationHandler = (event) =>{
      if(event.target.tagName == 'SPAN'){
    
        setSelectedSortByPopulation(event.target.id)
        setSelectedSortByArea()
        setSortByPopulationDropDown(false)
      }
    } 

    const sortByAreaHandler = (event)=> {
      if(event.target.tagName == 'SPAN'){
      
        setSelectedSortByArea(event.target.id)
        setSelectedSortByPopulation()
        setSortByAreaDropDown(false)
      }
    }

    const lightStyle = {
        backgroundColor: 'white',
        color: 'black'
    }
    const darkStyle = {
        backgroundColor: 'rgb(43, 57, 69)',
        color: 'white',
        boxShadow: "0 0 20px rgba(0, 0, 0, 0.5)" ,
    }

  return (
    <div className='dropdown' onClick={type_of_dropdown == 'population' ?sortByPopulationHandler : sortByAreaHandler } style={darkMode? darkStyle : lightStyle}>
        <span id='Increasing' className={`dropdown-option ${type_of_dropdown == 'population' ? (selectedSortByPopulation=='Increasing' ? "selected" : "") : (selectedSortByArea=='Increasing' ? "selected" : "")}`  }>Increasing</span>
        <span id="Decreasing" className={`dropdown-option ${type_of_dropdown == 'population' ? (selectedSortByPopulation=='Decreasing' ? "selected" : "") : (selectedSortByArea=='Decreasing' ? "selected" : "")}`}>Decreasing</span> 
    </div>
  )
}
