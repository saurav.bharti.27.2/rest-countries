import React, { useEffect, useState} from 'react'
import './Home.css'
import { useGlobalContext } from '../../context/GlobalContext'
import RegionalDropDown from '../regionalDropDown/RegionalDropDown'
import SubRegionalDropDown from '../subRegionalDropDown/SubRegionalDropDown'
import SortByDropDown from '../sortByDropDown/SortByDropDown'
import Card from '../cardComponent/Card'
import Spinner from '../spinner/Spinner'
import Error from '../errorComponent/Error'

export default function Home() {

    const [regionalDropDown, setRegionalDropDown] = useState(false)
    const [subRegionalDropDown, setSubRegionalDropDown] = useState(false)
    const [sortByPopulationDropDown, setSortByPopulationDropDown] = useState(false)
    const [sortByAreaDropDown, setSortByAreaDropDown] = useState(false)
    const [countryData, setCountryData] = useState([])
    const [searchInput, setSearchInput] = useState('')
    const [selectedRegion, setSelectedRegion] = useState('All')
    const [selectedSubRegion, setSelectedSubRegion]= useState('All')
    const [selectedSortByPopulation, setSelectedSortByPopulation] = useState()
    const [selectedSortByArea, setSelectedSortByArea] = useState()
    const [regions, setRegions] = useState({})

    let error = ''

    const {darkMode, setDarkMode, cca3CountryCode, setCAA3CountryCode} = useGlobalContext()

    const [loading, setLoading] = useState(true)
    
    const searchHandler = (event) => {
        setSearchInput(event.target.value)
    }
    const regionalDropDownHandler = ()=>{
        if(regionalDropDown){
            setRegionalDropDown(false)
        }else{
            setRegionalDropDown(true)
        }
    }
    const subRegionalDrowDownHandler = () => {
        if(subRegionalDropDown){
            setSubRegionalDropDown(false)
        }else{
            setSubRegionalDropDown(true)
        }
    }
    const sortByPopulationDropDownHandler = () => {
        if(sortByPopulationDropDown){
            setSortByPopulationDropDown(false);
        }else{
            setSortByPopulationDropDown(true)
        }
    }

    const sortByAreaDropDownHandler = () => {
        if(sortByAreaDropDown){
            setSortByAreaDropDown(false);
        }else{
            setSortByAreaDropDown(true);
        }
    }

    const getData = () => {
        
        fetch('https://restcountries.com/v3.1/all').then((response) => {
            return response.json()
        }).then((data) => {
            setCountryData(data)
            
            setLoading(false)

            let cca3Country= {}
            let tempRegions= {}
            data.map((country) => {
                cca3Country[country.cca3] = country.name.common  
                if(!tempRegions[country.region]){
                    tempRegions[country.region] = []
                }

                if(country.subregion){
                    if(!tempRegions[country.region].includes(country.subregion)){
                        tempRegions[country.region].push(country.subregion) 
                    }
                }
            })

            setCAA3CountryCode(cca3Country);
            setRegions(tempRegions)
        }).catch((err) => {
            setLoading(false)
            console.log(err.stack)
            error = ("Error: while fetching the data, please reload the page.")
        })
    }
    
    useEffect(() =>{
        getData()
    }, [])


    let selectedCountries = []

    function filterCountries(){

        countryData && countryData.map((country) => {
            
            if(country.name.common.toLowerCase().trim().includes(searchInput.toLowerCase().trim()) &&
                (selectedRegion=='All' || country.region.includes(selectedRegion) )  &&
                (  (selectedSubRegion == 'Other' && country.region == undefined)  || selectedSubRegion=='All' || (country.subregion!= undefined && country.subregion.includes(selectedSubRegion)))) {
                    selectedCountries.push(country)
            }
        } )
    
        if(selectedSortByPopulation){
            let temp = []
            
            selectedCountries.map((country) => {
                temp.push([Number(country.population), country])
            })
    
            if(selectedSortByPopulation == 'Increasing'){
                
                temp.sort((a,b) => a[0]-b[0])
                selectedCountries = []
                temp.forEach((countryData) => {
                    selectedCountries.push(countryData[1])
                })
    
            }else{
                temp.sort((a,b) => (b[0]-a[0]))
                selectedCountries = []
                temp.map((countryData) => {
                    selectedCountries.push(countryData[1])
                })
            }
    
        }
    
        if(selectedSortByArea){
            let temp = []
            
            selectedCountries.map((country) => {
                temp.push([Number(country.area), country])
            })
    
            if(selectedSortByArea == 'Increasing'){
                
                temp.sort((a,b) => a[0]-b[0])
                selectedCountries = []
                temp.forEach((countryData) => {
                    selectedCountries.push(countryData[1])
                })
    
            }else{
                temp.sort((a,b) => (b[0]-a[0]))
                selectedCountries = []
                temp.map((countryData) => {
                    selectedCountries.push(countryData[1])
                })
            }
        }
    
        if(selectedCountries.length>0){
            error= ''
        }else if(countryData.length>0 && selectedCountries.length<=0){
            error= (selectedRegion != 'All' ? `Error : No, Countries found with given name in ${selectedRegion} in ${selectedSubRegion} subregion.` : 'Error : No, Countries found with given name');
        }
    }

    filterCountries();
    



    const darkStyle ={
        backgroundColor: 'hsl(207, 26%, 17%)',
        color: 'white',
    }
    const lightStyle = {
        backgroundColor: 'white',
        color: 'black',
    }
    const darkFilterStyle = {
        backgroundColor: 'rgb(43, 57, 69)',
        color: 'white'
    }

  return (
    <div className='home' style={!darkMode ? lightStyle :darkStyle} >
        
        <div className="search_row">
            <div className="search" style={!darkMode? lightStyle : darkFilterStyle}>
            <i className={`fa-solid fa-magnifying-glass`}   ></i>
                <input placeholder="Search for a country.."  className={`search_country ${darkMode ? 'darkmode' : ''}`} value={searchInput} onChange={searchHandler} style={!darkMode? lightStyle : darkFilterStyle}  />

            </div>

            <div className='filter_container_subregion' style={darkMode ? {'boxShadow': "0 0 20px rgba(0, 0, 0, 0.5)" } : {}}>

                <div className='filter_by_subregion' style={!darkMode? lightStyle : darkFilterStyle}  onClick={subRegionalDrowDownHandler}>
                    <span >Filter by Sub-Region</span>
                    <i className="fa-solid fa-chevron-down" ></i>
                </div>
                {
                    subRegionalDropDown && 
                    <SubRegionalDropDown 
                        setSelectedSubRegion={setSelectedSubRegion}
                        selectedSubRegion= {selectedSubRegion}
                        selectedRegion= {selectedRegion}
                        setSubRegionalDropDown= {setSubRegionalDropDown}
                        regions= {regions}
                    />
                }
            </div>  

            <div className='filter_container_region' style={darkMode ? {'boxShadow': "0 0 20px rgba(0, 0, 0, 0.5)" } : {}}>

                <div className='filter_by_region' style={!darkMode? lightStyle : darkFilterStyle}  onClick={regionalDropDownHandler}>
                    <span >Filter by Region</span>
                    <i className="fa-solid fa-chevron-down" ></i>
                </div>
                {
                    regionalDropDown && 
                    <RegionalDropDown 
                        selectedRegion= {selectedRegion}
                        setSelectedRegion= {setSelectedRegion}
                        setRegionalDropDown= {setRegionalDropDown}
                        setSelectedSubRegion= {setSelectedSubRegion}
                        regions= {regions}
                    />
                }
            </div>
            
            <div className='sort_by_population_container' style={darkMode ? {'boxShadow': "0 0 20px rgba(0, 0, 0, 0.5)" } : {}}>

                <div className='sort_by_population' style={!darkMode? lightStyle : darkFilterStyle}  onClick={sortByPopulationDropDownHandler}>
                    <span >Sort by Population</span>
                    <i className="fa-solid fa-chevron-down" ></i>
                </div>
                {
                    sortByPopulationDropDown && 
                    <SortByDropDown 
                        type_of_dropdown="population"
                        setSelectedSortByPopulation={setSelectedSortByPopulation}
                        setSelectedSortByArea={setSelectedSortByArea}
                        setSortByPopulationDropDown={setSortByPopulationDropDown}
                        setSortByAreaDropDown={setSortByAreaDropDown}
                        selectedSortByPopulation={selectedSortByPopulation}
                        selectedSortByArea={selectedSortByArea}
                    />
                }
            </div>
            <div className='sort_by_area_container' style={darkMode ? {'boxShadow': "0 0 20px rgba(0, 0, 0, 0.5)" } : {}}>

                <div className='sort_by_area' style={!darkMode? lightStyle : darkFilterStyle}  onClick={sortByAreaDropDownHandler}>
                    <span >Sort by Area</span>
                    <i className="fa-solid fa-chevron-down" ></i>
                </div>
                {
                    sortByAreaDropDown && 
                    <SortByDropDown 
                        type_of_dropdown="area" 
                        setSelectedSortByPopulation={setSelectedSortByPopulation}
                        setSelectedSortByArea={setSelectedSortByArea}
                        setSortByPopulationDropDown={setSortByPopulationDropDown}
                        setSortByAreaDropDown={setSortByAreaDropDown}
                        selectedSortByPopulation={selectedSortByPopulation}
                        selectedSortByArea={selectedSortByArea}
                    />
                }
            </div>

        </div>

        {
            loading && <Spinner />
        }

        <div className='countries'>
            {
                selectedCountries && selectedCountries.map((country) => {
                    return (
                        <Card name= {country.name.common} region = {country.region} population={Number(country.population).toLocaleString()} capital={country.capital} flag_url={country.flags.png} id={country.ccn3} />
                        )
                })
            }
        </div>
        {
            error && <Error  error={error} />
        }
    </div>
  )
}
