import React from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { useEffect, useState } from 'react'
import Spinner from '../spinner/Spinner'
import './DetailPage.css'
import { useGlobalContext } from '../../context/GlobalContext'

export default function DetailPage() {

    const {id} = useParams() 
    const {darkMode, cca3CountryCode} = useGlobalContext()

    const [loading, setLoading] = useState(true);

    const [countryData, setCountryData] = useState()
    const [error, setError]= useState('')
    let nativeName = ''
    let currencies = ''
    let languages = []
    let country= ''
    

    const navigate= useNavigate()

    const getDetailedData =()=> {

        fetch(`https://restcountries.com/v3.1/alpha/${id}`).then((response) => {

            if(!response.ok){
                throw new Error()
            }

            return response.json()
        }).then((country) => {
            
            setCountryData(country[0])
            setLoading(false)
              
        }).catch((err) => {
            
            setError(`Error 404 : No countries found with CCN3 id - ${id}`)
            setLoading(false)
        })
    }

    if(countryData){
        country= countryData[0]
    
        let objNativeName = countryData.name.nativeName;
        let firstNativeName = Object.keys(objNativeName)[0];
        nativeName= objNativeName[firstNativeName].common
    
        let objCurrencies = countryData.currencies;
        let firstValueCurrency = Object.keys(objCurrencies)[0];
        currencies = (objCurrencies[firstValueCurrency].name)
    
        let currentLanguages = ''
        Object.entries(countryData.languages).map((language)=> {
            
            currentLanguages+=  language[1]
            currentLanguages+= ' '
        })
        languages = (currentLanguages)
        
    }
    
    

    useEffect(() => {
        getDetailedData()
    }, [])
    
    const goBackToHome = () => {
        navigate('/');
    }

  return (
    <div>
        {loading && <Spinner />}
        {
            (!loading && countryData) && 
            (
                <div className='detail_container'>
                    <div className='back_btn'>
                        <button className={`btn ${darkMode ? 'darkmode': ''}`} onClick={goBackToHome}> <i className="fa-solid fa-arrow-left fa-xl"></i>Back</button>
                    </div>
                    <div className='detail_main_container'>

                        <div className='flag'>
                            <img src={countryData.flags.png} />    
                            
                        </div>   
                        <div className="information_side">
                            <h1 className='country_name'>{countryData.name.common} </h1>
                            <div className='information'> 
                                <div className= 'left_information'> 
                                    <span className='native_name'> <h4>Native Name:</h4> {nativeName}</span>
                                    <span className='population_detail'> <h4>Population:</h4> {countryData.population.toLocaleString()}</span>
                                    <span className='region_detail'> <h4>Region:</h4> {countryData.region}</span>
                                    <span className='subregion'> <h4>Sub Region:</h4> {countryData.subregion}</span>
                                </div>
                                <div className='right_information'>
                                    <span className='top_level_domain'> <h4>Top Level Domain:</h4> {countryData.tld[0]}</span>
                                    <span className='currencies'> <h4>Currencies:</h4> {currencies}</span>
                                    <span className='languages'> <h4>Languages:</h4> {languages}</span>
                                </div>
                            </div>
                            <div className='border_container'> 
                                <span><h4>Border countries:</h4> </span>
                                {
                                    countryData.borders && countryData.borders.map((neighbourCode) => {
                                        return <span className='border_countries'>{cca3CountryCode[neighbourCode]}</span>
                                    })

                                }
                                {!countryData.borders && <span>&nbsp; None</span>}
                            </div>
                        </div> 
                    </div>
                </div>
            )
        }
        {
            error && <div className= 'error_div'>{error}</div>
        }
        
    </div>
  )
}
