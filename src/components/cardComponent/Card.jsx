import React from 'react'
import './Card.css'
import { useGlobalContext } from '../../context/GlobalContext';
import { useNavigate } from 'react-router-dom';
export default function Card(information) {

    const {name, population, region, capital, flag_url, id} = information;
    const {darkMode, setDarkMode} = useGlobalContext()

    const navigate = useNavigate()

    const cardStyleDark = {
      backgroundColor: 'hsl(209, 23%, 22%)',
      boxShadow: "0 0 20px rgba(0, 0, 0, 0.5)" ,
      color: 'white'
    }
    const cardStyleLight = {
      backgroundColor: 'white',
      color: 'black'
    }

    const cardClickHandler = (event) => {
      // console.log(event, event.target, event.target.id, event.target.parentElement, event.target.tagName)
      const id =  (event.target.tagName!='SPAN' ? (event.target.tagName != "UL" ?event.target.parentElement.id.slice(5) : event.target.id.slice(5)) : event.target.id.slice(5));
      
      navigate(`/country/${id}`);
    }


  return (
    <ul className='card' onClick={cardClickHandler} id={`card-${id}`} style={ !darkMode ? cardStyleLight : cardStyleDark} >
        <img src={flag_url}/>
        <li className='country_name'>{name}</li>
        <li className='population'><span id={`type-${id}`}>Population</span>: {population}</li>
        <li className='region'><span id={`type-${id}`}>Region</span>: {region}</li>
        <li className='capital'><span id={`type-${id}`}>Capital</span>: {capital}</li>
    </ul>
  )
}
