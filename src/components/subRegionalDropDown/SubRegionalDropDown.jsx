import React from 'react'
import './SubRegionalDropDown.css'
import { useGlobalContext } from '../../context/GlobalContext';

export default function SubRegionalDropDown({setSelectedSubRegion, selectedSubRegion, selectedRegion, setSubRegionalDropDown, regions}) {

    const {darkMode} = useGlobalContext() 

    const changeSubRegionHandler = (event) => {
        if(event.target.tagName == 'SPAN'){
      
            setSelectedSubRegion(event.target.id)
            setSubRegionalDropDown(false)
        }
    }

    const lightStyle = {
        backgroundColor: 'white',
        color: 'black'
    }
    const darkStyle = {
        backgroundColor: 'rgb(43, 57, 69)',
        color: 'white',
        boxShadow: "0 0 20px rgba(0, 0, 0, 0.5)" ,
    }
        

  return (
    <div className='dropdown subregional' onClick={changeSubRegionHandler} style={darkMode? darkStyle : lightStyle} >
        {   
            selectedRegion != 'All'
            &&
            regions[selectedRegion].map((subRegion) => {
                return <span id={subRegion}  className={`dropdown-option ${selectedSubRegion==subRegion ? "selected" : ""}`}>{subRegion}</span> 
            })
        }
        {
            selectedRegion == 'All'
            && 
            
            <>
                <span id='All'  className={`dropdown-option ${selectedSubRegion=='All' ? "selected" : ""}`}>All</span>
                
                {  
                
                    Object.values(regions).map((AllSubRegion) => {
                        let childs = AllSubRegion.map((subRegion) => {

                            return <span id={subRegion}  className={`dropdown-option ${selectedSubRegion==subRegion ? "selected" : ""}`}>{subRegion}</span> 
                        })
                        return childs
                    })
                }
            </>
        }
    </div>
  )
}
