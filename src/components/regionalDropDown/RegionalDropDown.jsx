import React from 'react'
import './RegionalDropDown.css'
import { useGlobalContext } from '../../context/GlobalContext'

export default function DropDown({selectedRegion, setSelectedRegion, setRegionalDropDown, setSelectedSubRegion, regions}) {

  const {darkMode, setDarkMode} = useGlobalContext()

  const changeRegionHandler = (event) => {
    if(event.target.tagName == 'SPAN'){
      
      setSelectedRegion(event.target.id)
      setRegionalDropDown(false)
      setSelectedSubRegion('All')
    }
  }

  const lightStyle = {
    backgroundColor: 'white',
    color: 'black'
  }
  const darkStyle = {
    backgroundColor: 'rgb(43, 57, 69)',
    color: 'white',
    boxShadow: "0 0 20px rgba(0, 0, 0, 0.5)" ,
  }
    
  return (
    <div className='dropdown' onClick={changeRegionHandler} style={darkMode? darkStyle : lightStyle}>
      <span id='All' className={`dropdown-option ${selectedRegion=='All' ? "selected" : ""}`}>All</span>
        {
          Object.keys(regions).map((region) => {
            return (
              <span id={region} className={`dropdown-option ${selectedRegion==region ? "selected" : ""}`}>{region}</span>
            )
          })
        }
    </div>
  )
}
