import React, { Fragment } from 'react'
import './Header.css'
import { useGlobalContext } from '../../context/GlobalContext'
import { Outlet } from 'react-router-dom'

export default function Header() {
  const {darkMode,setDarkMode} = useGlobalContext()

  const handleDarkMode = () => {
    if(darkMode){
      setDarkMode(false)
    }else{
      setDarkMode(true)
    }

  }
  
  const lightStyle= {
    backgroundColor: 'white',
    color: 'black',
    boxShadow: '0 0 10px rgba(0, 0, 0, 0.5)',
    
  }
  const darkStyle= {
    backgroundColor: 'hsl(209, 23%, 22%)',
    color: 'white',
    height: '100%'
  }

  return (
    <Fragment>
      <div className='navbar'>
        <div className='navbar_container'   style={!darkMode? lightStyle : darkStyle} >
            <h2>Where in the world</h2>
            <div className='change_background'>
                <i className="fa-regular fa-moon" onClick={handleDarkMode}></i>
                <button className={`${darkMode ? "dark_btn" : "light_btn"}`} onClick={handleDarkMode}> Dark Mode</button>
            </div>
        </div>

      </div>
      <Outlet />
    </Fragment>
  )
}
