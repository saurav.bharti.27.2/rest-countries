
import './App.css'
import Header from './components/header/Header'
import Home from './components/home/Home'
import DetailPage from './components/detailPage/DetailPage'
import { useGlobalContext } from './context/GlobalContext'
import {Routes, Route} from 'react-router-dom'

function App() {
  
  const {darkMode, setDarkMode} = useGlobalContext()

  const lightStyle= {
    backgroundColor: 'white',
    color: 'black'

  }
  const darkStyle= {
    backgroundColor: 'rgb(32, 44, 55)',
    color: 'white'
  }

 
  return (
    <div id="container" style={!darkMode? lightStyle : darkStyle}>
      <Routes>
        <Route path='/' element={< Header />} >
          <Route index element={< Home />} />
          <Route path='/country/:id' element={< DetailPage />} />
        </Route>
      </Routes>
    </div>
  )
}

export default App
