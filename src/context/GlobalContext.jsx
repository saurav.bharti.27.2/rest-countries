
import React, { useContext,useState } from "react";
import { createContext } from "react";

const GlobalContext = React.createContext()

export function useGlobalContext(){
    return useContext(GlobalContext)
}

export function GlobalProvider({children}){

    const [darkMode, setDarkMode] = useState(false)
    const [cca3CountryCode, setCAA3CountryCode] = useState({})
    
    const value= {
        darkMode,
        setDarkMode,
        cca3CountryCode,
        setCAA3CountryCode
    }

    return <GlobalContext.Provider value={value}>{children}</GlobalContext.Provider>
}